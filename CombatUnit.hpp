#ifndef COMBATUNIT_HPP
#define COMBATUNIT_HPP

#include <vector>
#include <iostream>
#include <string>
#include "Item.hpp"
//#include "Spawnable.hpp"
#include "Spell.hpp"
//class Spell;
//class Item;

class CombatUnit{
public:

    CombatUnit(std::string name, int health, int damage, int healing);
    std::string GetName();
    int GetHealth();
    int GetDamage();
    int GetHealing();
//    void UseItem(Item* item, CombatUnit* target);
//    void UsePotion(Potion* item, CombatUnit* target);   //split into use *each item* to load function
//    void UseBigPotion(BigPotion* item, CombatUnit* target);   //split into use *each item* to load function
//    void UseGrenade(Grenade* item, CombatUnit* target);   //split into use *each item* to load function
    void ModifyHealth(int amount);
    void ModifyDamage(int amount);
    void DealDamage(CombatUnit* target);
    void Heal();
//    void CastSpell(Spell* spell, CombatUnit* target);
//    void CastFireballSpell(FireballSpell* spell, CombatUnit* target);   //split into cast *each spell* to load function
//    void CastHealSpell(HealSpell* spell, CombatUnit* target);   //split into cast *each spell* to load function
//    void CastBlindSpell(BlindSpell* spell, CombatUnit* target);   //split into cast *each spell* to load function
    void PrintStats();
//    void PrintInventory();
//    Item* GetInventory(int);
//    int GetInventorySize();
    bool IsTired();
    void Tired();
    bool IsBlind();
    void Blind();
    void Refresh();
    int GetMaxHealth();
    //virtual AND  = 0;

protected:
    std::string name;
    int health;
    int maxHealth;
    int damage;
    int healing = 0;
    bool tired;
    bool blind;
//    std::vector<Item*> inventory;   //moved to subclasses
//    std::vector<Spell*> spells;   //moved to subclasses
};

#endif // COMBATUNIT_HPP
