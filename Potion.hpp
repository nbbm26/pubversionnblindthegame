#ifndef POTION_HPP
#define POTION_HPP

#include "Item.hpp"
#include "CombatUnit.hpp"
#include "GameTile.hpp"
//#include "Spawnable.hpp"

class Potion : public Item {
public:
    Potion();
    void Effect(CombatUnit* target);
    void Spawn(GameTile* GameTile);
//    void Effect(CombatUnit* target) = 0;   //added from spell to load functionally
};

#endif // POTION_HPP
