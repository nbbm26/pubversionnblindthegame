#include "BigPotion.hpp"

BigPotion::BigPotion() : Item("BigPotion", "Restores 20hp to the target.") {
    this->type = "BigPotion";

}

void BigPotion::Effect(CombatUnit* target){
    target->ModifyHealth(-20);
}

void BigPotion::Spawn(GameTile* GameTile){
    GameTile->AddItem(new BigPotion());
}
