#ifndef PLAYER_HPP
#define PLAYER_HPP

#include "Item.hpp"
#include "CombatUnit.hpp"
#include "Spell.hpp"
//#include <vector>

class Player : public CombatUnit {
public:
    Player(std::string name, int health, int damage, int healing, std::string style);
    void AddItemToInventory(Item* item);
    void PrintInventory();
    Item* GetInventory(int);
    int GetInventorySize();
    std::string GetStyle();
    void SetStyle(std::string);
    void CastSpell(Spell* spell, CombatUnit* target);
//    void CastFireballSpell(FireballSpell* spell, CombatUnit* target);   //split into cast *each spell* to load function
//    void CastHealSpell(HealSpell* spell, CombatUnit* target);   //split into cast *each spell* to load function
//    void CastBlindSpell(BlindSpell* spell, CombatUnit* target);   //split into cast *each spell* to load function
    void UseItem(Item* item, CombatUnit* target);
//    void UsePotion(Potion* item, CombatUnit* target);   //split into use *each item* to load function
//    void UseBigPotion(BigPotion* item, CombatUnit* target);   //split into use *each item* to load function
//    void UseGrenade(Grenade* item, CombatUnit* target);   //split into use *each item* to load function
//    std::string getName();
    bool GetIsWallopped();
    void SetIsWallopped(bool wallopped);
private:
    std::string name;
    std::string style;
    std::vector<Item*> inventory;// = {BigPotion,Grenade,Potion};   //moved to subclasses
    bool isWallopped;
    //removed learning spells for now
//    std::vector<Spell*> spells;// = {BlindSpell,HealSpell,FireballSpell};   //moved to subclasses
};

#endif // PLAYER_HPP
