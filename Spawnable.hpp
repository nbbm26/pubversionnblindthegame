#ifndef SPAWNABLE_HPP
#define SPAWNABLE_HPP

#include <vector>
#include <iostream>
#include <string>

//class GameTile;

class Spawnable {
public:
    Spawnable();
//    virtual void Spawn(GameTile* GameTile) = 0;
    std::string GetType();
protected:
    std::string type;
};

#endif // SPAWNABLE_HPP
