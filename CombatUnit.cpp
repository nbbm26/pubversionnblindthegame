#include "CombatUnit.hpp"

CombatUnit::CombatUnit(std::string name, int health, int damage, int healing){
    this->name = name;
    this->health = health;
    this->damage = damage;
    this->healing = healing;
    this->maxHealth = health*1.3;
}

int CombatUnit::GetHealth() {
    return this->health;
}

int CombatUnit::GetMaxHealth() {
    return this->maxHealth;
}

int CombatUnit::GetDamage() {
    return this->damage;
}

int CombatUnit::GetHealing() {
    return this->healing;
}

std::string CombatUnit::GetName() {
    return this->name;
}

void CombatUnit::PrintStats(){
    std::cout<<this->name<<"'s health: "<<this->health<<"\tDamage: "<<this->damage<<std::endl<<std::endl;
}

void CombatUnit::ModifyHealth(int amount){
    this->health -= amount;
}

void CombatUnit::ModifyDamage(int amount){
    this->damage -= amount;
    if (this->damage<0)
        this->damage=0;
}

void CombatUnit::DealDamage(CombatUnit* target){
    std::cout<<this->name <<" attacks!"<<std::endl;
    target->ModifyHealth(this->damage);
}

void CombatUnit::Heal(){
    std::cout<< this->name <<" regenerates " <<this->healing << "HP" <<std::endl;
    this->ModifyHealth((0-1)*(this->healing));
}

//void CombatUnit::UseItem(Item* item, CombatUnit* target){     //moved to subclasses
//    std::cout<<this->name<<" uses "<<item->GetName()<<std::endl;
//    int i = 0;
//    for (Item* item : this->inventory){
//        if (this->GetInventory(i)->GetName()==item->GetName())
//            this->inventory.erase(this->inventory.begin() + i);
//        else
//            i++;
//    }
//    item->Effect(target);
//}

//void CombatUnit::CastSpell(Spell* spell, CombatUnit* target){
//    std::cout<<this->name<<" casts "<<spell->GetName()<<std::endl;
//    this->tired = true;
//    spell->SpellEffect(target);
//}

//int CombatUnit::GetInventorySize(){     //moved to subclasses
//    return this->inventory.size();
//}

//Item* CombatUnit::GetInventory(int ref){
//    return this->inventory.at(ref);
//}

//void CombatUnit::PrintInventory(){
// for (Item* item : this->inventory)
//     std::cout<< item->GetName()<< item->GetDescription() << std::endl;
//}

bool CombatUnit::IsTired(){
    return this->tired;
}

void CombatUnit::Tired(){
    this->tired = true;
}

bool CombatUnit::IsBlind(){
    return this->blind;
}

void CombatUnit::Blind(){
    this->blind = true;
}

void CombatUnit::Refresh(){
    this->tired = false;
}
