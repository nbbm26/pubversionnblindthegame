#ifndef GAMETILE_HPP
#define GAMETILE_HPP

#include "CombatUnit.hpp"
#include "Player.hpp"
//#include "Enemy.hpp"
//#include "Potion.hpp"
//#include "BigPotion.hpp"
//#include "Grenade.hpp"
#include "Item.hpp"
//#include "GreenMoss.hpp"
//#include "Orc.hpp"
//#include "OrcHealer.hpp"
//#include "Goblin.hpp"
//#include "GoblinFireMage.hpp"
//#include "Ogre.hpp"
//#include "OgreMage.hpp"
#include "Spell.hpp"
//#include "Island.hpp"
//#include "FireballSpell.hpp"
//#include "HealSpell.hpp"
//#include "BlindSpell.hpp"
#include <vector>

class GameTile {
public:
    GameTile(Player* player);
    void AddEnemy(CombatUnit* enemy);
    void AddItem(Item* item);
    void AddSpell(Spell* spell);
    void ListEnemies();
    void ListItems();
    char GetIsland();
    void SetIsland(char island);
    bool GetHasSpawned();
    void SetHasSpawned(bool state);
    bool GetBossDed();
    void SetBossDed(bool state);
    Player* GetPlayer();
    void SetPlayer(Player* player);
    void SimulateCombat();
private:
    Player* player;
    char island = 'A';
    bool hasSpawned = false;
    bool bossDed = false;
    std::vector<CombatUnit*> enemiesInTile; //changed from enemy to Combat unit type
    std::vector<Item*> itemsInTile;
    std::vector<Spell*> spellsInTile;
};

#endif // GAMETILE_HPP
