#include "GameWorld.hpp"


GameWorld::GameWorld(GameTile* gametile)
{
    this->gametile = gametile;
    this->AddAdjacentIsland('A', 'B', 5);
    this->AddAdjacentIsland('B', 'C', 3);
    this->AddAdjacentIsland('A', 'D', 7);
    this->AddAdjacentIsland('B', 'M', 5);
    this->AddAdjacentIsland('F', 'Z', 3);
    this->AddAdjacentIsland('D', 'M', 7);
    this->AddAdjacentIsland('E', 'H', 5);
    this->AddAdjacentIsland('C', 'E', 3);
    this->AddAdjacentIsland('G', 'E', 7);
    this->AddAdjacentIsland('J', 'U', 5);
    this->AddAdjacentIsland('U', 'P', 3);
    this->AddAdjacentIsland('U', 'O', 7);
    this->AddAdjacentIsland('F', 'B', 5);
    this->AddAdjacentIsland('F', 'H', 3);
    this->AddAdjacentIsland('F', 'O', 7);
    this->AddAdjacentIsland('X', 'U', 5);
    this->AddAdjacentIsland('B', 'J', 3);
    this->AddAdjacentIsland('J', 'K', 7);
    this->AddAdjacentIsland('T', 'Z', 5);
    this->AddAdjacentIsland('T', 'Y', 3);
    this->AddAdjacentIsland('T', 'M', 7);
    this->AddAdjacentIsland('N', 'G', 7);
    this->AddAdjacentIsland('E', 'O', 5);
    this->AddAdjacentIsland('V', 'P', 3);
    this->AddAdjacentIsland('P', 'O', 7);
    this->AddAdjacentIsland('T', 'Z', 5);
    this->AddAdjacentIsland('T', 'Y', 3);
    this->AddAdjacentIsland('T', 'M', 7);
    this->AddAdjacentIsland('E', 'V', 5);
    this->AddAdjacentIsland('E', 'I', 3);
    this->AddAdjacentIsland('I', 'J', 7);
    this->AddAdjacentIsland('B', 'A', 5);
    this->AddAdjacentIsland('C', 'B', 3);
    this->AddAdjacentIsland('D', 'A', 7);
    this->AddAdjacentIsland('M', 'B', 5);
    this->AddAdjacentIsland('Z', 'F', 3);
    this->AddAdjacentIsland('M', 'D', 7);
    this->AddAdjacentIsland('H', 'E', 5);
    this->AddAdjacentIsland('E', 'C', 3);
    this->AddAdjacentIsland('E', 'G', 7);
    this->AddAdjacentIsland('U', 'J', 5);
    this->AddAdjacentIsland('P', 'U', 3);
    this->AddAdjacentIsland('O', 'U', 7);
    this->AddAdjacentIsland('B', 'F', 5);
    this->AddAdjacentIsland('H', 'F', 3);
    this->AddAdjacentIsland('O', 'F', 7);
    this->AddAdjacentIsland('U', 'X', 5);
    this->AddAdjacentIsland('J', 'B', 3);
    this->AddAdjacentIsland('K', 'J', 7);
    this->AddAdjacentIsland('Z', 'T', 5);
    this->AddAdjacentIsland('Y', 'T', 3);
    this->AddAdjacentIsland('M', 'T', 7);
    this->AddAdjacentIsland('G', 'N', 7);
    this->AddAdjacentIsland('O', 'E', 5);
    this->AddAdjacentIsland('P', 'V', 3);
    this->AddAdjacentIsland('O', 'P', 7);
    this->AddAdjacentIsland('Z', 'T', 5);
    this->AddAdjacentIsland('Y', 'T', 3);
    this->AddAdjacentIsland('M', 'T', 7);
    this->AddAdjacentIsland('V', 'E', 5);
    this->AddAdjacentIsland('I', 'E', 3);
    this->AddAdjacentIsland('J', 'I', 7);
}

GameTile* GameWorld::GetGameTile(){
    return this->gametile;
}

void GameWorld::AddIsland(int key){                     //add new Island to GameWorld
    Island_list_.push_back(new Island(key));
    num_Islands_++;
}


void GameWorld::AddAdjacentIsland(char from, char to, int weight){
    Island *vfrom = GetIsland(from);
    Island *vto = GetIsland(to);
    vfrom->AddAdjacentIsland(vto, weight);                    //adds AdjacentIsland from origin to destination, gives weight
}


Island* GameWorld::GetIsland(char key){
    int i = 0;
    while(i < num_Islands_){                   //to loop through Island_list
        if(Island_list_[i]->GetKey() == key){   //returns Island from list if one matches key given
            return Island_list_[i];
        }
        i++;                                    //increments key
    }

    AddIsland(key);
    return Island_list_[i];
}

std::vector<char> GameWorld::GetChoices(){
    std::vector<char> choices;
    Island *currIsland = GetIsland(this->GetGameTile()->GetIsland());
    std::cout << "Choices are from " << currIsland->GetKey() << " to: ";
    for(int i=0; i<currIsland->GetNumAdjacentIslands(); i++){
        if(currIsland->HasAdjacentIsland(currIsland->GetAdjacentIsland(i)->GetTo()->GetKey()))
            choices.push_back(currIsland->GetAdjacentIsland(i)->GetTo()->GetKey());

        //        if(currIsland->HasAdjacentIsland(currIsland->GetAdjacentIsland(i)->GetTo()->GetKey()))  //requires HasAdjacentIsland to work, haven't gotten it to
        //            return Island_list_[i];
        //        return nullptr;
    }
    return choices;
}

bool GameWorld::IsAdjacent(char from, char to){
    Island *vfrom = GetIsland(from);
    Island *vto = GetIsland(to);
    //HasAdjacentIsland expects a character, not a Island pointer
    //Also, be sure to put this in an if and return the appropriate boolean value

    char vecfrom = vfrom->GetKey();
    char vecto = vto->GetKey();
    std::cout << "from " << vecfrom << " to " << vecto << "||||";
    if (vfrom->HasAdjacentIsland(vecto))        //hits error here
        return true;
    return false;
}

void GameWorld::PrintNeighbors(char island){
    Island *currIsland = GetIsland(island);
    std::cout << "Neighbors of " << currIsland->GetKey() << ": ";
    for(int i=0; i<currIsland->GetNumAdjacentIslands(); i++){
        if(currIsland->HasAdjacentIsland(currIsland->GetAdjacentIsland(i)->GetTo()->GetKey()))
            std::cout << currIsland->GetAdjacentIsland(i)->GetTo()->GetKey() << ",";

        //        if(currIsland->HasAdjacentIsland(currIsland->GetAdjacentIsland(i)->GetTo()->GetKey()))  //requires HasAdjacentIsland to work, haven't gotten it to
        //            return Island_list_[i];
        //        return nullptr;
    }
}

void GameWorld::AvailableMoves(){
    Island *currIsland = GetIsland(this->gametile->GetIsland());
    std::cout << "Islands close enough to jump from " << currIsland->GetKey() << " are: ";
    for(int i=0; i<currIsland->GetNumAdjacentIslands(); i++){
        if(currIsland->HasAdjacentIsland(currIsland->GetAdjacentIsland(i)->GetTo()->GetKey()))
            std::cout << currIsland->GetAdjacentIsland(i)->GetTo()->GetKey();
        if (i!= currIsland->GetNumAdjacentIslands()-1)
            std::cout << ",";
    }
    std::cout << std::endl;
}

void GameWorld::Spawn(){

    GameTile* tile = this->gametile;
    while (tile->GetHasSpawned()==false){
        Spawnable* spawnables[6];// = new Spawnable*[4];
            // for(int i=0;i<6 ;i++){
//                for(int j=0;j<50;j++){
//                    for(int k=0;k<50;k++){
                        // spawnables[i] = new GreenMoss();
//                    }
//                }
            // }
        //    newp = rand()%100+1;
        //     if (newp < 30)
        int newp = rand()%100+1;
        if (tile->GetIsland()!='G'){
            if (newp<15)
                spawnables[0] = new BigPotion();
            else
                spawnables[0] = new GreenMoss();
            if (newp<30)
                spawnables[1] = new Potion();
            else
                spawnables[1] = new GreenMoss();
            newp = rand()%100+1;
            if (newp<20)
                spawnables[2] = new Grenade();
            else
                spawnables[2] = new GreenMoss();
            newp = rand()%100+1;
            if (newp<25)
                spawnables[3] = new OrcHealer();
            else if (newp<50&&newp>=25)
                spawnables[3] = new Orc();
            else
                spawnables[3] = new GreenMoss();
            newp = rand()%100+1;
            if (newp<35)
                spawnables[4] = new GoblinFireMage();
            else if (newp<70&&newp>=35)
                spawnables[4] = new Goblin();
            else
                spawnables[4] = new GreenMoss();
            newp = rand()%100+1;
            if (newp<20)
                spawnables[5] = new OgreMage();
            else if (newp<40&&newp>=20)
                spawnables[5] = new Ogre();
            else
                spawnables[5] = new GreenMoss();

            //    int counters[6] = {1, 1, 1, 1, 1, 1};

            int currSpawnable = 0;
            for(Spawnable* spawner : spawnables){
                //        for(int i = 0; counters[currSpawnable] < 6; i++){
                if(spawner->GetType()=="BigPotion"){
                    BigPotion* usingSpawner = new BigPotion();
                    usingSpawner->Spawn(tile);
                    continue;}
                else if(spawner->GetType()=="Grenade"){
                    Grenade* usingSpawner = new Grenade();
                    usingSpawner->Spawn(tile);
                    continue;}
                else if(spawner->GetType()=="Potion"){
                    Potion* usingSpawner = new Potion();
                    usingSpawner->Spawn(tile);
                    continue;}
                else if(spawner->GetType()=="Orc"){
                    Orc* usingSpawner = new Orc();
                    usingSpawner->Spawn(tile);
                    continue;}
                else if(spawner->GetType()=="OrcHealer"){
                    OrcHealer* usingSpawner = new OrcHealer();
                    usingSpawner->Spawn(tile);
                    continue;}
                else if(spawner->GetType()=="Goblin"){
                    Goblin* usingSpawner = new Goblin();
                    usingSpawner->Spawn(tile);
                    continue;}
                else if(spawner->GetType()=="GoblinFireMage"){
                    GoblinFireMage* usingSpawner = new GoblinFireMage();
                    usingSpawner->Spawn(tile);
                    continue;}
                else if(spawner->GetType()=="Ogre"){
                    Ogre* usingSpawner = new Ogre();
                    usingSpawner->Spawn(tile);
                    continue;}
                else if(spawner->GetType()=="OgreMage"){
                    OgreMage* usingSpawner = new OgreMage();
                    usingSpawner->Spawn(tile);
                    continue;}
                else/* if(spawner->GetType()=="GreenMoss")*/{
                    GreenMoss* usingSpawner = new GreenMoss();
                    usingSpawner->Spawn(tile);
                    continue;}
                //            usingSpawner->Spawn(tile);
                //        }
                currSpawnable++;
            }
            //        hasSpawned = 1;
        }
        else{
        	if (tile->GetBossDed()==false){
     	    	spawnables[0] = new BegPaOgre();
        	    spawnables[1] = new GreenMoss();
            	spawnables[2] = new GreenMoss();
            	spawnables[3] = new GreenMoss();
            	spawnables[4] = new GreenMoss();
            	spawnables[5] = new GreenMoss();

            	int currSpawnable = 0;
            	for(Spawnable* spawner : spawnables){
                //        for(int i = 0; counters[currSpawnable] < 6; i++){
                	if(spawner->GetType()=="BegPaOgre"){
                    	BegPaOgre* usingSpawner = new BegPaOgre();
                    	usingSpawner->Spawn(tile);
                    	continue;}
                	else if(spawner->GetType()=="GreenMoss"){
                    	GreenMoss* usingSpawner = new GreenMoss();
                    	usingSpawner->Spawn(tile);
                    	continue;}
               		else if(spawner->GetType()=="Goblin"){
                    	Goblin* usingSpawner = new Goblin();
                    	usingSpawner->Spawn(tile);
                    	continue;}
                	currSpawnable++;
            	}
        	}
    	}
        tile->SetHasSpawned(true);
    }
}
void GameWorld::PlayerActionPack(){
    while(true){
        std::cout << "What do you want to do? (Look, Search, Move, Inventory, Stats, or Rest)" << std::endl;
        std::string playerAction = "";
        std::getline(std::cin, playerAction);
        if (playerAction == "Look"){
            std::cout << "The stars look very pretty with all the islands floating softly amidst the backdrop." << std::endl << std::endl;
            this->AvailableMoves();
        }
        else if (playerAction == "Search"){
            srand(time(0));
            if (rand()%100 < 5){
                std::cout << "You found a Potion!" << std::endl << std::endl;
            	std::cout << "Take? (y or n)" << std::endl;
            	char playerAction2 = ' ';
            	while(playerAction2!='-'){
                	std::cin.get(playerAction2);
                	if (playerAction2 == 'y' || playerAction2 == 'n'){
                		if (playerAction2 == 'y'){
                			this->GetGameTile()->GetPlayer()->AddItemToInventory(new Potion);
                        	playerAction2 = '-';
                		}
                		else
                			std::cout << "You drop the Potion and it rolls under some green moss." << std::endl;
                	}
                	else{
                    std::cout << "That was not a valid decision." << std::endl;
                	}
            	}
            }
            else if (rand()%100 < 5){
                std::cout << "You found a Grenade!" << std::endl << std::endl;
            	std::cout << "Take? (y or n)" << std::endl;
            	char playerAction2 = ' ';
            	while(playerAction2!='-'){
                	std::cin.get(playerAction2);
                	if (playerAction2 == 'y' || playerAction2 == 'n'){
                		if (playerAction2 == 'y'){
                			this->GetGameTile()->GetPlayer()->AddItemToInventory(new Grenade);
                        	playerAction2 = '-';
                		}
                		else
                			std::cout << "You drop the Grenade and it rolls under some green moss." << std::endl;
                	}
                	else{
                    std::cout << "That was not a valid decision." << std::endl;
                	}
            	}
            }
            else if (rand()%100 < 5){
                std::cout << "You found a BigPotion!" << std::endl << std::endl;
            	std::cout << "Take? (y or n)" << std::endl;
            	char playerAction2 = ' ';
            	while(playerAction2!='-'){
                	std::cin.get(playerAction2);
                	if (playerAction2 == 'y' || playerAction2 == 'n'){
                		if (playerAction2 == 'y'){
                			this->GetGameTile()->GetPlayer()->AddItemToInventory(new BigPotion);
                        	playerAction2 = '-';
                		}
                		else
                			std::cout << "You drop the BigPotion and it rolls under some green moss." << std::endl;
                	}
                	else{
                    std::cout << "That was not a valid decision." << std::endl;
                	}
            	}
            }
            else
	            std::cout << "You feel around but all that is found is green moss." << std::endl << std::endl;
        }
        else if (playerAction == "Move"){
            std::cout << "Which Island?" << std::endl;
            char playerAction2 = ' ';
            std::vector<char> choices = this->GetChoices();
            while(playerAction2!='-'){
                this->AvailableMoves();
                std::cin.get(playerAction2);
                for (char choice : choices){
                    if (playerAction2 == choice){
                        this->GetGameTile()->SetIsland(choice);
                        playerAction2 = '-';
                        return;
                    }
                    else
                        std::cout << "That was not a valid decision." << std::endl;
                }
            }
        }
        else if (playerAction == "Inventory"){
            this->GetGameTile()->GetPlayer()->PrintInventory();
            std::cout << std::endl;
        }
        else if (playerAction == "Stats"){
            this->GetGameTile()->GetPlayer()->PrintStats();
            std::cout << std::endl;
        }
        else if (playerAction == "Rest"){
            std::cout << "You burrow and bury your stuff in the soft green moss and hope nobody comes." << std::endl;
            while (this->GetGameTile()->GetPlayer()->GetHealth()<this->GetGameTile()->GetPlayer()->GetMaxHealth())
                this->GetGameTile()->GetPlayer()->ModifyHealth(-1);
        }
        else{
            std::cout << "That was answer was not understood" << std::endl;
        }
    }
}
