#ifndef TILE_HPP
#define TILE_HPP

#include "Enemy.hpp"
#include "Item.hpp"
#include "Spell.hpp"
#include "GameWorld.hpp"
#include "Spawnable.hpp"
#include <vector>

class Tile{
public:
    Tile();
    void AddEnemy();
    void AddItem();
    void AddSpell();
    void ListEnemies();
    void ListItems();
    void ListSpells();
private:
    std::vector<Enemy*> enemiesInTile;
    std::vector<Item*> itemsInTile;
    std::vector<Spell*> spellsInTile;
#endif // TILE_HPP

