#ifndef FIREBALLSPELL_HPP
#define FIREBALLSPELL_HPP

#include "Spell.hpp"
#include "CombatUnit.hpp"
#include "GameTile.hpp"

class FireballSpell : public Spell {
public:
    FireballSpell();
    void SpellEffect(CombatUnit* target);
    void Spawn(GameTile* GameTile);
//    void SpellEffect(CombatUnit* target) = 0;   //added from spell to load functionally
};

#endif // FIREBALLSPELL_HPP
