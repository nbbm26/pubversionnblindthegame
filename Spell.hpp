#ifndef SPELL_HPP
#define SPELL_HPP

//#include <string>
//#include <iostream>
#include "Spawnable.hpp"
//#include "CombatUnit.hpp"
//#include "GameTile.hpp"

//class CombatUnit;
//class Spawnable;

class Spell : public Spawnable {
public:
    Spell(std::string name, std::string description);
    std::string GetName();
    std::string GetDescription();
    void PrintData();
//    virtual void SpellEffect(CombatUnit* target) = 0;
protected:
    std::string name;
    std::string description;
};

#endif // SPELL_HPP
