#ifndef GREENMOSS_HPP
#define GREENMOSS_HPP

#include "Item.hpp"
//#include "CombatUnit.hpp"
#include "GameTile.hpp"

class GreenMoss : public Item {
public:
    GreenMoss();
    void Effect(CombatUnit* target);
    void Spawn(GameTile* GameTile);
//    void Effect(CombatUnit* target) = 0;   //added from spell to load functionally
};

#endif // GREENMOSS_HPP

