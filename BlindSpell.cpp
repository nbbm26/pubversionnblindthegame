#include "BlindSpell.hpp"
#include "CombatUnit.hpp"
#include "GameTile.hpp"


BlindSpell::BlindSpell() : Spell("BlindSpell", "Target can't do damage.") {
    this->type = "BlindSpell";

}

void BlindSpell::SpellEffect(CombatUnit* target){
    target->ModifyDamage(50);
}

void BlindSpell::Spawn(GameTile* GameTile){
    GameTile->AddSpell(new BlindSpell());
}
