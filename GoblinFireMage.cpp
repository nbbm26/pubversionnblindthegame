#include "GoblinFireMage.hpp"

GoblinFireMage::GoblinFireMage() : Enemy(this->name, this->health, this->damage, this->healing){
    this->type = "GoblinFireMage";
}

GoblinFireMage::GoblinFireMage(std::string name, int health, int damage, int healing) : Enemy(name, health, damage, healing){
    this->type = "GoblinFireMage";

}

void GoblinFireMage::Spawn(GameTile* GameTile){
    GameTile->AddEnemy(new GoblinFireMage(this->name, this->health, this->damage, this->healing));
}
