#ifndef BIGPOTION_HPP
#define BIGPOTION_HPP

#include "Item.hpp"
#include "CombatUnit.hpp"
#include "GameTile.hpp"

class BigPotion : public Item {
public:
    BigPotion();
    void Effect(CombatUnit* target);
    void Spawn(GameTile* GameTile);
//    void Effect(CombatUnit* target) = 0;   //added from spell to load functionally
};

#endif // BIGPOTION_HPP
