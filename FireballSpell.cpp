#include "FireballSpell.hpp"
#include "CombatUnit.hpp"
#include "GameTile.hpp"


FireballSpell::FireballSpell() : Spell("FireballSpell", "Burns 8hp to the target.") {
    this->type = "FireballSpell";

}

void FireballSpell::SpellEffect(CombatUnit* target){
    target->ModifyHealth(8);
}

void FireballSpell::Spawn(GameTile* GameTile){
    GameTile->AddSpell(new FireballSpell());
}
