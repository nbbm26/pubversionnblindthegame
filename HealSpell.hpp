#ifndef HEALSPELL_HPP
#define HEALSPELL_HPP

#include "Spell.hpp"
//#include "CombatUnit.hpp"
#include "GameTile.hpp"

class HealSpell : public Spell {
public:
    HealSpell();
    void SpellEffect(CombatUnit* target);
    void Spawn(GameTile* GameTile);
//    void SpellEffect(CombatUnit* target) = 0;   //added from spell to load functionally
};

#endif // HEALSPELL_HPP
