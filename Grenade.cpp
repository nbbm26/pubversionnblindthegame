#include "Grenade.hpp"

Grenade::Grenade() : Item("Grenade", "Damages the targets for 15hp.") {
    this->type = "Grenade";

}

void Grenade::Effect(CombatUnit* target){
    target->ModifyHealth(15);
}

void Grenade::Spawn(GameTile* GameTile){
    GameTile->AddItem(new Grenade());
}
