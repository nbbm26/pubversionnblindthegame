#ifndef FEMALEOgreMage_HPP
#define FEMALEOGREMAGE_HPP

//#include "CombatUnit.hpp"
//#include "Ogre.hpp"
#include "Enemy.hpp"
#include "GameTile.hpp"

//class Ogre;

class FemaleOgreMage : public Enemy {
public:
    FemaleOgreMage();
    FemaleOgreMage(std::string name, int health, int damage, int healing);
    void Spawn(GameTile* GameTile);
private:
    const std::string name = "FemaleOgreMage";
    const int health = 24;
    const int damage = 12;
    const int healing = 8;
};

#endif // FEMALEOGREMAGE_HPP

