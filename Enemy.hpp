#ifndef ENEMY_HPP
#define ENEMY_HPP

#include "Spawnable.hpp"
#include "CombatUnit.hpp"
#include "Item.hpp"
#include "Spell.hpp"
#include "GameTile.hpp"

//class CombatUnit;

class Enemy : public Spawnable, public CombatUnit {
public:
    Enemy(std::string name, int health, int damage, int healing);
    void CastSpell(Spell* spell, CombatUnit* target);
//    void CastFireballSpell(FireballSpell* spell, CombatUnit* target);   //split into cast *each spell* to load function
//    void CastHealSpell(HealSpell* spell, CombatUnit* target);   //split into cast *each spell* to load function
//    void CastBlindSpell(BlindSpell* spell, CombatUnit* target);   //split into cast *each spell* to load function
//    std::string getName();
    void PrintInventory();
    Item* GetInventory(int);
    int GetInventorySize();
    void UseItem(Item* item, CombatUnit* target);
//    void UsePotion(Potion* item, CombatUnit* target);   //split into use *each item* to load function
//    void UseBigPotion(BigPotion* item, CombatUnit* target);   //split into use *each item* to load function
//    void UseGrenade(Grenade* item, CombatUnit* target);   //split into use *each item* to load function
protected:
    std::vector<Item*> inventory;// = {BigPotion,Grenade,Potion};   //moved to subclasses
    std::vector<Spell*> spells;// = {BlindSpell,HealSpell,FireballSpell};   //moved to subclasses
//	std::string name;
};

#endif // ENEMY_HPP
