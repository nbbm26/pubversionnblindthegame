#include "BegPaOgre.hpp"


BegPaOgre::BegPaOgre() : Enemy(this->name, this->health, this->damage, this->healing) {
    this->type = "BegPaOgre";

}

BegPaOgre::BegPaOgre(std::string name, int health, int damage, int healing) : Enemy(name, health, damage, healing){
    this->type = "BegPaOgre";

}

void BegPaOgre::Spawn(GameTile* GameTile){
    GameTile->AddEnemy(new BegPaOgre(this->name, this->health, this->damage, this->healing));
}

