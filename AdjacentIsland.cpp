#include "AdjacentIsland.hpp"

AdjacentIsland::AdjacentIsland()
{

}

AdjacentIsland::AdjacentIsland(Island *to, int weight){
    to_ = to;
    weight_ = weight;
}

Island* AdjacentIsland::GetTo(){
    return to_;
}

int AdjacentIsland::GetWeight(){
    return weight_;
}
