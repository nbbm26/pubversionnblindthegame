#include "Enemy.hpp"
#include "FireballSpell.hpp"
#include "HealSpell.hpp"
#include "BlindSpell.hpp"
#include "Potion.hpp"
#include "BigPotion.hpp"
#include "Grenade.hpp"

Enemy::Enemy(std::string name, int health, int damage, int healing): Spawnable(), CombatUnit(name, health, damage, healing){
    this->name = name;
    this->health = health;
    this->damage = damage;
    this->healing = healing;
}

int Enemy::GetInventorySize(){     //moved to subclasses
    return this->inventory.size();
}

Item* Enemy::GetInventory(int ref){
    return this->inventory.at(ref);
}

void Enemy::PrintInventory(){
 for (Item* item : this->inventory)
     std::cout<< item->GetName()<< item->GetDescription() << std::endl;
}

void Enemy::UseItem(Item* item, CombatUnit* target){
    std::cout<<this->name<<" uses "<<item->GetName()<<std::endl;
    int i = 0;
    for (Item* item : this->inventory){
        if (this->GetInventory(i)->GetName()==item->GetName()){
            this->inventory.erase(this->inventory.begin() + i);
            break;}
        else
            i++;
    }
    if(item->GetName()=="BigPotion"){
        BigPotion* usingItem = new BigPotion();
        usingItem->Effect(target);}
    else if(item->GetName()=="Grenade"){
        Grenade* usingItem = new Grenade();
        usingItem->Effect(target);}
//    if(item->GetName()=="Potion")
    else{
        Potion* usingItem = new Potion();
        usingItem->Effect(target);}
//    item->Effect(target);
}

void Enemy::CastSpell(Spell* spell, CombatUnit* target){
    std::cout<<this->name<<" casts "<<spell->GetName()<<std::endl;
    this->tired = true;
    if(spell->GetName()=="BlindSpell"){
        BlindSpell* usingSpell = new BlindSpell();
        usingSpell->SpellEffect(target);}
    else if(spell->GetName()=="FireballSpell"){
        FireballSpell* usingSpell = new FireballSpell();
        usingSpell->SpellEffect(target);}
    else{
//    if(spell->GetName()=="HealSpell")
        HealSpell* usingSpell = new HealSpell();
        usingSpell->SpellEffect(target);}
//    spell->SpellEffect(target);
}

//std::string Enemy::getName() {
//    return this->name;
//}
