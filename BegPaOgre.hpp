#ifndef BEGPAOGRE_HPP
#define BEGPAOGRE_HPP

//#include "CombatUnit.hpp"
//#include "Ogre.hpp"
#include "Enemy.hpp"
#include "GameTile.hpp"

//class Ogre;

class BegPaOgre : public Enemy {
public:
    BegPaOgre();
    BegPaOgre(std::string name, int health, int damage, int healing);
    void Spawn(GameTile* GameTile);
private:
    const std::string name = "BegPaOgre";
    const int health = 40;
    const int damage = 12;
    const int healing = 3;
};

#endif // BEGPAOGRE_HPP

