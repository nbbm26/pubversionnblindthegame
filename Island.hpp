#ifndef ISLAND_H
#define ISLAND_H

#include <iostream>
#include <vector>
#include "AdjacentIsland.hpp"

//class AdjacentIsland;

class Island
{
public:
    Island();
    Island(int key);

    char GetKey();
    int GetNumAdjacentIslands();
    AdjacentIsland* GetAdjacentIsland(char to);
    AdjacentIsland* GetAdjacentIsland(int index);

    void AddAdjacentIsland(Island *to, int key);
    bool HasAdjacentIsland(char to);
private:
    char key_;
    int num_AdjacentIslands_ = 0;
    std::vector<AdjacentIsland *> AdjacentIslands_;
};

#endif // ISLAND_H
