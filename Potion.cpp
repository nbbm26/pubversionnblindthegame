#include "Potion.hpp"

Potion::Potion() : Item("Potion", "Restores 5hp to the target.") {
    this->type = "Potion";

}

void Potion::Effect(CombatUnit* target){
    target->ModifyHealth(-5);
}

void Potion::Spawn(GameTile* GameTile){
    GameTile->AddItem(new Potion());
}
