#include "Goblin.hpp"

Goblin::Goblin() : Enemy(this->name, this->health, this->damage, this->healing){
    this->type = "Goblin";
}

Goblin::Goblin(std::string name, int health, int damage, int healing) : Enemy(name, health, damage, healing){

}

void Goblin::Flee(){
    std::cout<<"The goblin flees!"<<std::endl;
}

void Goblin::Spawn(GameTile* GameTile){
    GameTile->AddEnemy(new Goblin(this->name, this->health, this->damage, this->healing));
}
