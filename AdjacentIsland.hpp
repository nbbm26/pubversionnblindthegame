#ifndef ADJACENTISLAND_H
#define ADJACENTISLAND_H

#include <iostream>
#include <vector>

class Island;

class AdjacentIsland
{
public:
    AdjacentIsland();
    AdjacentIsland(Island *to, int weight);
    Island* GetTo();
    int GetWeight();
private:
    Island *to_;
    int weight_ = 0;
};

#endif // ADJACENTISLAND_H
