#ifndef GAMEWORLD_H
#define GAMEWORLD_H

#include <iostream>
#include <iomanip>
#include <vector>
#include "Island.hpp"
#include "AdjacentIsland.hpp"
#include "GameTile.hpp"
#include "FireballSpell.hpp"
#include "HealSpell.hpp"
#include "BlindSpell.hpp"
#include "Potion.hpp"
#include "BigPotion.hpp"
#include "GreenMoss.hpp"
#include "Grenade.hpp"
#include "Ogre.hpp"
#include "OgreMage.hpp"
#include "Orc.hpp"
#include "OrcHealer.hpp"
#include "Goblin.hpp"
#include "GoblinFireMage.hpp"
#include "BegPaOgre.hpp"
#include "FemaleOgreMage.hpp"

class GameWorld
{
public:
    GameWorld(GameTile* gametile);
    void AddIsland(int key);
    void AddAdjacentIsland(char to, char from, int weight);

    Island* GetIsland(char label);
    std::vector<char> GetChoices();

    bool IsAdjacent(char from, char to);
    void PrintNeighbors(char Island);
    void AvailableMoves();
    GameTile* GetGameTile();
    void PlayerActionPack();
    void Spawn();

private:
    GameTile* gametile;
    int num_Islands_ = 0;
    std::vector<Island*> Island_list_;
};

#endif // GAMEWORLD_H
