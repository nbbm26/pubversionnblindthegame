#include "Spawnable.hpp"
#include "Item.hpp"
#include "Spell.hpp"
#include "CombatUnit.hpp"
#include "Player.hpp"
#include "GameTile.hpp"
#include "FireballSpell.hpp"
#include "HealSpell.hpp"
#include "BlindSpell.hpp"
#include "Potion.hpp"
#include "BigPotion.hpp"
#include "Grenade.hpp"
#include "GreenMoss.hpp"
#include "Enemy.hpp"
#include "Orc.hpp"
#include "OrcHealer.hpp"
#include "Goblin.hpp"
#include "GoblinFireMage.hpp"
#include "Ogre.hpp"
#include "OgreMage.hpp"
#include "Island.hpp"
#include "AdjacentIsland.hpp"
#include "GameWorld.hpp"
#include <vector>
#include <iostream>
#include <string>
#include <limits>

int main(){

    std::cout << "You have found your self at the edge of the known Universe!  You don't know how you got here, but the sight is amazing!  Around swirl stars";
    std::cout << " brightly shining while huge islands of floating rock delicately drift suspended in space.  Wisps of smoke trail around you";
    std::cout << ", multi-colored gasses dance back and forth and the firm rock somehow contains air you can breathe.  Off in the distance a";
    std::cout << " bright white pillar of light shines against the darkness, perhaps with some key as to your whereabouts and more information";
    std::cout << " about the environment you now find your self in." << std::endl << std::endl;
    std::cout << "As you look around you notice some shapes hopping between the islands, telling you there's a way to travel and...";
    std::cout << "you aren't the only creature here!  Watch out and choose wisely, for many decisions may lead you to a strange new world,";
    std::cout << " a pool of dead, or perhaps a pretty fluffy cloud you can call your own!" << std::endl << std::endl;
    std::cout << "It seems pretty clear that some islands are close enough that traversing is possible.  And, it seems the only sensible direction";
    std::cout << " to go is towards the Tall White Fountain of Energy burning dreadfully distinct against the Darkness of the Night.";
    std::cout << " Dare you dream of electric sheep and what waits beyond?" << std::endl << std::endl;
    std::cout << "[A notebook of pen and paper is suggested to map your way through the islands]" << std::endl << std::endl;

//    char currIsland = 'A';

    srand(time(0));
//    int newp = rand()%100+1;
    int playerHealth = 40;
    int playerDamage = 5;
    int playerHealing = 0;
    std::string playerName = "Joe";
    std::string playerType = "Player";
    std::cout << "What is your player's name?" << std::endl;
    std::cin.clear();
    /*std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');*/
    std::getline(std::cin, playerName);
    std::cout << "What is your player's type?" << std::endl;
    std::cout << "Mage[default] has all spells but lower health (think Dr. Strange)" << std::endl;
    std::cout << "Healer has high health and healing (think Deadpool(c)" << std::endl;
    std::cout << "Fighter has high health and damage (think Cloud)" << std::endl;
    std::cin.clear();
    /*std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');*/
    std::getline(std::cin, playerType);
    if(playerType=="Healer"){
        playerHealth=60;
        playerHealing=5;
        playerDamage = 7;
    }
    else if(playerType=="Fighter"){
        playerHealth=50;
        playerDamage=12;
    }
    else
        playerType="Mage";
    Player* Player1 = new Player(playerName, playerHealth, playerDamage, playerHealing, playerType);

    GameTile* Blind = new GameTile(Player1);
    GameWorld new_GameWorld(Blind);
    new_GameWorld.AvailableMoves();
    new_GameWorld.Spawn();
    new_GameWorld.GetGameTile()->SimulateCombat();

    while(new_GameWorld.GetGameTile()->GetIsland()!='N'){
        std::cin.clear();
        /*std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');*/
        if (new_GameWorld.GetGameTile()->GetPlayer()->GetHealth() <1)
            new_GameWorld.GetGameTile()->GetPlayer()->SetIsWallopped(true);
        if (new_GameWorld.GetGameTile()->GetPlayer()->GetIsWallopped()==true)
            return 0;
        while (new_GameWorld.GetGameTile()->GetPlayer()->GetHealth() > new_GameWorld.GetGameTile()->GetPlayer()->GetMaxHealth())
            new_GameWorld.GetGameTile()->GetPlayer()->ModifyHealth(1);
        new_GameWorld.AvailableMoves();
        new_GameWorld.GetGameTile()->SetHasSpawned(false);
        if (new_GameWorld.GetGameTile()->GetIsland()!='A'){
            new_GameWorld.Spawn();
            new_GameWorld.GetGameTile()->SimulateCombat();
        }
        new_GameWorld.PlayerActionPack();
    }
    if (new_GameWorld.GetGameTile()->GetIsland()=='N'){
        std::cout << "The Tall White Fountain burns brightly";
        std::cout << " before you.  You gasp at it's warm glow and ";
        std::cout << "feel a sense of inner peace drawing you in.  Your ";
        std::cout << "whole essence begins to flow easily into the glow ";
        std::cout << "and you are drawn upward, awakening a thousand times";
        std::cout << " as your body forms the words: ";
        return 0;
    }
}
