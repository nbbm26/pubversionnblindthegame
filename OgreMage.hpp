#ifndef OGREMAGE_HPP
#define OGREMAGE_HPP

//#include "CombatUnit.hpp"
//#include "Ogre.hpp"
#include "Enemy.hpp"
#include "GameTile.hpp"

//class Ogre;

class OgreMage : public Enemy {
public:
    OgreMage();
    OgreMage(std::string name, int health, int damage, int healing);
    void Spawn(GameTile* GameTile);
private:
    const std::string name = "OgreMage";
    const int health = 24;
    const int damage = 12;
    const int healing = 8;
};

#endif // OGREMAGE_HPP

