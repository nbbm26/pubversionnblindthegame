#include "Island.hpp"


Island::Island(){
    key_ = '?';
    num_AdjacentIslands_ = 0;
}


Island::Island(int key)
{
   key_ = key;
}


char Island::GetKey(){
    return key_;
}


int Island::GetNumAdjacentIslands(){
    return num_AdjacentIslands_;
}


AdjacentIsland* Island::GetAdjacentIsland(char to){
    for(int i = 0; i < num_AdjacentIslands_; i++){            //loop through AdjacentIslands
        if(AdjacentIslands_[i]->GetTo()->GetKey() == to){
            return AdjacentIslands_[i];                       //returns AdjacentIsland number that connects to specified Island
        }
    }
    return 0;
}


AdjacentIsland* Island::GetAdjacentIsland(int index){       //asks for AdjacentIsland of a specific number
    if(index < num_AdjacentIslands_){             //returns AdjacentIsland if specified number within
        return AdjacentIslands_[index];
    }
    return nullptr;
}


void Island::AddAdjacentIsland(Island *to, int weight){
    AdjacentIslands_.push_back(new AdjacentIsland(to, weight));       //add AdjacentIsland,specify its weight and where it connects to
    num_AdjacentIslands_++;                                 //update number of AdjacentIslands for Island
}


bool Island::HasAdjacentIsland(char to){                    //checks if Island has AdjacentIsland with other Island
    for(int i = 0; i < num_AdjacentIslands_; i++){
        if(AdjacentIslands_[i]->GetTo()->GetKey() == to){
            return true;
        }
    }
    return false;
}
