#include "Spell.hpp"

Spell::Spell(std::string name, std::string description){
    this->name = name;
    this->description = description;
}

std::string Spell::GetName(){
    return this->name;
}

std::string Spell::GetDescription(){
    return this->description;
}

void Spell::PrintData(){
    std::cout<<this->name<<"\t"<<this->description<<std::endl;
}
