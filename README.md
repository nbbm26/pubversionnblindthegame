# NBlind
A nice new game based around a non-linear maze with a cool combat ui!  Try it and see!

Very recent, polished in that it works, utilizing the Graphster and many acquired skills, though better organization of objects could have been pulled off.  Time constraints limited this, but even so, it's working and hopefully enjoyable!  One of my later projects I'm rather happy on how it turned out!

Can provide a console based game framework and be fleshed out in terms of items, enemies, spells, and abilities.  For now, has 3 basic classes, a handful of enemies, a few spells, few abilites and a small range of items.

No leveling currently, though that would be cool if balanced correctly.
