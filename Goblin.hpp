#ifndef GOBLIN_HPP
#define GOBLIN_HPP

#include "Enemy.hpp"
#include "GameTile.hpp"

class Goblin : public Enemy {
public:
    Goblin();
    Goblin(std::string name, int health, int damage, int healing);
    void Flee();
    void Spawn(GameTile* GameTile);
private:
    const std::string name = "Goblin";
    const int health = 6;
    const int damage = 2;
    const int healing = 0;
};

#endif // GOBLIN_HPP
