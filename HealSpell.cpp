#include "HealSpell.hpp"
#include "CombatUnit.hpp"
#include "GameTile.hpp"


HealSpell::HealSpell() : Spell("HealSpell", "Restores 8hp to the target.") {
    this->type = "HealSpell";

}

void HealSpell::SpellEffect(CombatUnit* target){
    target->ModifyHealth(-15);
}

void HealSpell::Spawn(GameTile* GameTile){
    GameTile->AddSpell(new HealSpell());
}
