#ifndef OGRE_HPP
#define OGRE_HPP

#include "Enemy.hpp"
#include "GameTile.hpp"

class Ogre : public Enemy {
public:
    Ogre();
    Ogre(std::string name, int health, int damage, int healing);
    void Bash();
    void Spawn(GameTile* GameTile);
private:
    const std::string name = "Ogre";
    const int health = 20;
    const int damage = 6;
    const int healing = 0;
};

#endif // OGRE_HPP

