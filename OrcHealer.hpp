#ifndef ORCHEALER_HPP
#define ORCHEALER_HPP

//#include "CombatUnit.hpp"
#include "Enemy.hpp"
//#include "Orc.hpp"
#include "GameTile.hpp"

//class Orc;

class OrcHealer : public Enemy {
public:
    OrcHealer();
    OrcHealer(std::string name, int health, int damage, int healing);
    void Spawn(GameTile* GameTile);
private:
    const std::string name = "OrcHealer";
    const int health = 12;
    const int damage = 4;
    const int healing = 6;
};

#endif // ORCHEALER_HPP
