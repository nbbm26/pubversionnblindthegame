#ifndef ITEM_HPP
#define ITEM_HPP

//#include <string>
//#include <iostream>
#include "Spawnable.hpp"
//#include "CombatUnit.hpp"
//#include "GameTile.hpp"

// class CombatUnit;

class Item : public Spawnable {
public:
    Item(std::string name, std::string description);
    std::string GetName();
    std::string GetDescription();
    void PrintData();
//    virtual void Effect(CombatUnit* target) = 0;
protected:
    std::string name;
    std::string description;
};

#endif // ITEM_HPP
