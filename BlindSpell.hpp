#ifndef BLINDSPELL_HPP
#define BLINDSPELL_HPP

#include "Spell.hpp"
#include "CombatUnit.hpp"
#include "GameTile.hpp"

class BlindSpell : public Spell {
public:
    BlindSpell();
    void SpellEffect(CombatUnit* target);
    void Spawn(GameTile* GameTile);
//    void SpellEffect(CombatUnit* target) = 0;   //added from spell to load functionally
};

#endif // BLINDSPELL_HPP
