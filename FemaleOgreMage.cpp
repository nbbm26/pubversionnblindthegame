#include "FemaleOgreMage.hpp"

FemaleOgreMage::FemaleOgreMage() : Enemy(this->name, this->health, this->damage, this->healing) {
    this->type = "FemaleOgreMage";

}

FemaleOgreMage::FemaleOgreMage(std::string name, int health, int damage, int healing) : Enemy(name, health, damage, healing){
    this->type = "FemaleOgreMage";

}

void FemaleOgreMage::Spawn(GameTile* GameTile){
    GameTile->AddEnemy(new FemaleOgreMage(this->name, this->health, this->damage, this->healing));
}


