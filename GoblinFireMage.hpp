#ifndef GOBLINFIREMAGE_HPP
#define GOBLINFIREMAGE_HPP

//#include "CombatUnit.hpp"
#include "Enemy.hpp"
//#include "Goblin.hpp"
#include "GameTile.hpp"

//class Goblin;

class GoblinFireMage : public Enemy {
public:
    GoblinFireMage();
    GoblinFireMage(std::string name, int health, int damage, int healing);
    void Spawn(GameTile* GameTile);
private:
    const std::string name = "GoblinFireMage";
    const int health = 8;
    const int damage = 6;
    const int healing = 0;
};

#endif // GOBLINFIREMAGE_HPP
