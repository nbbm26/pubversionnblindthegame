#ifndef GRENADE_HPP
#define GRENADE_HPP

#include "Item.hpp"
//#include "CombatUnit.hpp"
#include "GameTile.hpp"

class Grenade : public Item {
public:
    Grenade();
    void Effect(CombatUnit *tile);
    void Spawn(GameTile* GameTile);
//    void Effect(CombatUnit* target) = 0;   //added from spell to load functionally
};

#endif // GRENADE_HPP
