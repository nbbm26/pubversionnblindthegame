#include "GreenMoss.hpp"

GreenMoss::GreenMoss() : Item("GreenMoss", "Softly glowing green moss.") {
    this->type = "GreenMoss";

}

void GreenMoss::Effect(CombatUnit* target){
    target->ModifyHealth(0);
}

void GreenMoss::Spawn(GameTile* GameTile){
    GameTile->AddItem(new GreenMoss());
}
