#include "Tile.hpp"

Tile::Tile(){}

void Tile::AddEnemy(Enemy* enemy) {
    this->enemiesInTile.push_back(enemy);
}

void Tile::AddItem(Item* item) {
    this->itemsInTile.push_back(item);
}

void Tile::AddSpell(Spell* spell) {
    this->spellsInTile.push_back(spell);
}

void Tile::ListEnemies() {
    int counter = 1;
    for (Enemy* enemy : this->enemiesInTile) {
        std::cout << "Enemy " << counter << ": ";
        enemy->PrintStats();
        if (enemy->GetHealth()<1){
            this->enemiesInTile.erase(this->enemiesInTile.begin()+(counter-1));
            std::cout<< "Enemy " << counter << " has fallen!" << std::endl << std::endl;
        }
//            this->enemiesInTile.erase(std::remove(enemiesInTile.begin(), enemiesInTile.end(), this->enemiesInTile[(counter-1)], enemiesInTile.end());
        counter++;
    }
}

void Tile::ListItems() {
    int counter = 1;
    for (Item* item : this->itemsInTile) {
        std::cout << "Item " << counter << ": ";
        item->PrintData();
        counter++;
    }
    std::cout << std::endl;
}

void Tile::ListSpells() {
    int counter = 1;
    for (Spell* spell : this->spellsInTile) {
        std::cout << "Spell " << counter << ": ";
        Spell->PrintData();
        counter++;
    }
    std::cout << std::endl;
}
