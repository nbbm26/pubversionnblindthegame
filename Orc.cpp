#include "Orc.hpp"

Orc::Orc() : Enemy(this->name, this->health, this->damage, this->healing) {
    this->type = "Orc";

}

Orc::Orc(std::string name, int health, int damage, int healing) : Enemy(name, health, damage, healing){
    this->type = "Orc";

}

void Orc::BullRush(){
    std::cout<<"The orc rushes!"<<std::endl;
}

void Orc::Spawn(GameTile* GameTile){
    GameTile->AddEnemy(new Orc(this->name, this->health, this->damage, this->healing));
}
