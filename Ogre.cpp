#include "Ogre.hpp"

Ogre::Ogre() : Enemy(this->name, this->health, this->damage, this->healing) {
    this->type = "Ogre";

}

Ogre::Ogre(std::string name, int health, int damage, int healing) : Enemy(name, health, damage, healing){
    this->type = "Ogre";

}

void Ogre::Bash(){
    std::cout<<"The Ogre bashes!"<<std::endl;
}

void Ogre::Spawn(GameTile* GameTile){
    GameTile->AddEnemy(new Ogre(this->name, this->health, this->damage, this->healing));
}
