CFLAGS+= -g all: Course

Course:  Main.cpp GameTile.cpp Spawnable.cpp Spell.cpp FireballSpell.cpp HealSpell.cpp BlindSpell.cpp Item.cpp Potion.cpp Grenade.cpp BigPotion.cpp GreenMoss.cpp CombatUnit.cpp Player.cpp Enemy.cpp Orc.cpp OrcHealer.cpp Goblin.cpp GoblinFireMage.cpp Ogre.cpp OgreMage.cpp BegPaOgre.cpp FemaleOgreMage.cpp GameWorld.cpp Island.cpp AdjacentIsland.cpp
	g++ Main.cpp GameTile.cpp Spawnable.cpp Spell.cpp FireballSpell.cpp HealSpell.cpp BlindSpell.cpp Item.cpp Potion.cpp Grenade.cpp BigPotion.cpp GreenMoss.cpp CombatUnit.cpp Player.cpp Enemy.cpp Orc.cpp OrcHealer.cpp Goblin.cpp GoblinFireMage.cpp Ogre.cpp OgreMage.cpp BegPaOgre.cpp FemaleOgreMage.cpp GameWorld.cpp Island.cpp AdjacentIsland.cpp -std=c++11 -o NBlind
