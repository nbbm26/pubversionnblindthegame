#include "OrcHealer.hpp"

OrcHealer::OrcHealer() : Enemy(this->name, this->health, this->damage, this->healing) {
    this->type = "OrcHealer";

}

OrcHealer::OrcHealer(std::string name, int health, int damage, int healing) : Enemy(name, health, damage, healing){
    this->type = "OrcHealer";

}

void OrcHealer::Spawn(GameTile* GameTile){
    GameTile->AddEnemy(new OrcHealer(this->name, this->health, this->damage, this->healing));
}
