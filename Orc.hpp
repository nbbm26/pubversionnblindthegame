#ifndef ORC_HPP
#define ORC_HPP

#include "Enemy.hpp"
#include "GameTile.hpp"

class Orc : public Enemy {
public:
    Orc();
    Orc(std::string name, int health, int damage, int healing);
    void BullRush();
    void Spawn(GameTile* GameTile);
private:
    const std::string name = "Orc";
    const int health = 10;
    const int damage = 4;
    const int healing = 0;
};

#endif // ORC_HPP
