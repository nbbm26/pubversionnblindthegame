#include "OgreMage.hpp"

OgreMage::OgreMage() : Enemy(this->name, this->health, this->damage, this->healing) {
    this->type = "OgreMage";

}

OgreMage::OgreMage(std::string name, int health, int damage, int healing) : Enemy(name, health, damage, healing){
    this->type = "OgreMage";

}

void OgreMage::Spawn(GameTile* GameTile){
    GameTile->AddEnemy(new OgreMage(this->name, this->health, this->damage, this->healing));
}

