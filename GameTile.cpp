#include "GameTile.hpp"
#include "FireballSpell.hpp"
#include "HealSpell.hpp"
#include "BlindSpell.hpp"
#include "Potion.hpp"
#include "BigPotion.hpp"
#include "GreenMoss.hpp"
#include "Grenade.hpp"
#include "Ogre.hpp"
#include "OgreMage.hpp"
#include "Orc.hpp"
#include "OrcHealer.hpp"
#include "Goblin.hpp"
#include "GoblinFireMage.hpp"
#include <limits>

GameTile::GameTile(Player* player) {
    this->player = player;
}

void GameTile::AddEnemy(CombatUnit* enemy) {
    this->enemiesInTile.push_back(enemy);
}

void GameTile::AddItem(Item* item) {
    this->itemsInTile.push_back(item);
}

void GameTile::AddSpell(Spell* spell) {
    this->spellsInTile.push_back(spell);
}

void GameTile::ListEnemies() {
    int counter = 1;
    for (CombatUnit* enemy : this->enemiesInTile) {
        std::cout << "Enemy " << counter << ": ";
        enemy->PrintStats();
        if (enemy->GetHealth()<1){
            this->enemiesInTile.erase(this->enemiesInTile.begin()+(counter-1));
            std::cout<< "Enemy " << counter << " has fallen!" << std::endl << std::endl;
            if (enemy->GetName()=="BegPaOgre")
            {
            	this->SetBossDed(true);
            }
        }
        //            this->enemiesInTile.erase(std::remove(enemiesInTile.begin(), enemiesInTile.end(), this->enemiesInTile[(counter-1)], enemiesInTile.end());
        counter++;
    }
}

void GameTile::ListItems() {
    int counter = 1;
    for (Item* item : this->itemsInTile) {
        std::cout << "Item " << counter << ": ";
        item->PrintData();
        counter++;
    }
    std::cout << std::endl;
}

Player* GameTile::GetPlayer(){
    return this->player;
}

bool GameTile::GetBossDed(){
    return this->bossDed;
}

void GameTile::SetBossDed(bool state){
    this->bossDed = state;
}

bool GameTile::GetHasSpawned(){
    return this->hasSpawned;
}

void GameTile::SetHasSpawned(bool state){
    this->hasSpawned = state;
}

void GameTile::SetPlayer(Player* player){
    this->player = player;
}

char GameTile::GetIsland(){
    return this->island;
}

void GameTile::SetIsland(char island){
    this->island = island;
}

void GameTile::SimulateCombat() {
    std::cin.clear();
    /*std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');*/

    std::cout << "\t\tFIGHT!" << std::endl<<std::endl;
    int round = 1;
    int playerTired = 0;
    int charTired = 0;
    int bossTired = 0;
    int run = 0;
    while (this->enemiesInTile.size()>0 && this->player->GetIsWallopped()==false) {
        std::cout << "\t\tPlayer & Lists" << std::endl;
        std::cout << "\t----Round " << round << "----" << std::endl;
        round++;
        if (playerTired>0)
            playerTired--;
        if (playerTired==0)
            this->player->Refresh();
        if (charTired>0)
            charTired--;
        if (bossTired>0)
            bossTired--;
        this->player->PrintStats();
        this->ListItems();
        this->ListEnemies();
        for (CombatUnit* enemy : this->enemiesInTile){
            if (round%2==1)
                enemy->Refresh();}

        std::string decision = "";
        long i_decision=0;
        long t_decision=0;
        int chcker = 0;
        while (i_decision!=5){  //player combat choices
            std::cin.clear();
            /*std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');*/
            std::cout << "What do you do?" << std::endl<<std::endl;
            std::cout << "Enter 1 to attack." << std::endl;
            std::cout << "Enter 2 cast spell." << std::endl;
            std::cout << "Enter 3 to use item." << std::endl;
            std::cout << "Enter 4 run away." << std::endl;
            std::cout << "Enter 5 to wait." << std::endl << std::endl;
            //	std::cout << "Enter 6 to exit the Program." << std::endl;

            while (chcker != 1){
                std::cin.clear();
                /*std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');*/
                std::getline(std::cin, decision);
                if(decision == "1" || decision == "2" || decision == "3" || decision == "4" || decision == "5"){//seems was slipped in when there isn't a decision 6
                    i_decision = std::stol(decision,nullptr,10);
                    chcker = 1;
                }
                else
                    std::cout << "That was not a valid choice.";
            }
            chcker = 0;
            switch (i_decision)
            {
            //		case 6:
            //			return 0;
            case 5:
                break;
            case 4:
                if (i_decision == 4){
                    run = 1;}
                i_decision = 5;
                break;
            case 3:     //player item choices
                if (i_decision == 3){
                    while (i_decision!=4 && i_decision!=5){
                        std::cout << "Which item?" << std::endl;
                        std::cout << "Enter 1 for Potion." << std::endl;
                        std::cout << "Enter 2 for Grenade." << std::endl;
                        std::cout << "Enter 3 for BigPotion." << std::endl;
                        std::cout << "Enter 4 to go back." << std::endl;

                        while (chcker != 1){
                            std::cin.clear();
                            /*std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');*/
                            std::getline(std::cin, decision);
                            if(decision == "1" || decision == "2" || decision == "3" || decision == "4"){
                                t_decision = std::stol(decision,nullptr,10);
                                chcker = 1;
                            }
                            else
                                std::cout << "That was not a valid choice.";
                        }
                        chcker = 0;
                        switch (t_decision)
                        {
                        case 4:
                            break;
                        case 3:     //big potion
                            for (int i = 0; i<this->player->GetInventorySize();i++) {
                                if (this->player->GetInventory(i)->GetName()=="BigPotion"){
                                    this->player->UseItem(new BigPotion, this->player);
                                    std::cout << this->player->GetName() << " feels MUCH better." << std::endl;
                                    if (this->player->GetInventorySize() > 0){
                                        this->player->PrintInventory();
                                    }
                                    i_decision = 5;
                                    break;}
                            }
                            std::cout << this->player->GetName() << " feels around but doesn't find a BigPotion" << std::endl;
                                    if (this->player->GetInventorySize() > 0){
                                        this->player->PrintInventory();
                                    }
                            i_decision = 5;
                            break;
                        case 2: //grenade
                            for (int i = 0; i<this->player->GetInventorySize();i++) {
                                if (this->player->GetInventory(i)->GetName()=="Grenade"){
                                    this->player->UseItem(new Grenade, this->player);
                                    while (chcker != 1){
                                        std::cout << "Which enemy?" << std::endl;
                                        this->ListEnemies();
                                        std::getline(std::cin, decision);
                                        if(decision == "1" || decision == "2" || decision == "3" || decision == "4" || decision == "5"){
                                            t_decision = std::stol(decision,nullptr,10);
                                            if (t_decision<=this->enemiesInTile.size())
                                                chcker = 1;
                                            else
                                                std::cout << "That was not a valid number of an enemy.";
                                        }
                                        else
                                            std::cout << "That was not a valid number of an enemy.";
                                    }
                                    //Player attacks Enemy
                                    this->player->UseItem(new Grenade(), this->enemiesInTile[t_decision-1]);
                                    std::cout << this->enemiesInTile[t_decision-1]->GetName() << " is blasted!" << std::endl;
                                    for (CombatUnit* enemy : this->enemiesInTile) {
                                        enemy->ModifyHealth(8);
                                    }
                                    if (this->player->GetInventorySize() > 0){
                                        this->player->PrintInventory();
                                    }
                                    this->ListEnemies();
                                    i_decision = 5;
                                    chcker = 0;
                                    break;}
                            }
                            std::cout << this->player->GetName() << " feels around but doesn't find a Grenade" << std::endl;
                                    if (this->player->GetInventorySize() > 0){
                                        this->player->PrintInventory();
                                    }
                            i_decision = 5;
                            break;
                        case 1: //potion
                            for (int i = 0; i<this->player->GetInventorySize();i++) {
                                if (this->player->GetInventory(i)->GetName()=="Potion"){
                                    this->player->UseItem(new Potion, this->player);
                                    std::cout << this->player->GetName() << " feels better." << std::endl;
                                    if (this->player->GetInventorySize() > 0){
                                        this->player->PrintInventory();
                                    }
                                    i_decision = 5;
                                    break;}
                            }
                            std::cout << this->player->GetName() << " feels around but doesn't find a Potion" << std::endl;
                                    if (this->player->GetInventorySize() > 0){
                                        this->player->PrintInventory();
                                    }
                            i_decision = 5;
                            break;
                        default:
                            std::cout << "That was not 1, 2, or 3." << std::endl;
                            break;}
                    }
                }
                break;
            case 2:     //player spell choices
                if (i_decision == 2){
                    while (i_decision!=4 && i_decision!=5){
                        std::cin.clear();
                        /*std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');*/
                        if(this->player->GetStyle() != "Mage"){
                            std::cout << "Mages...gotta love em...too bad you're not one, eh?" << std::endl;
                            break;
                        }
                        std::cout << "Which spell?" << std::endl;
                        std::cout << "Enter 1 for fireball." << std::endl;
                        std::cout << "Enter 2 for heal." << std::endl;
                        std::cout << "Enter 3 for blind." << std::endl;
                        std::cout << "Enter 4 to go back." << std::endl;

                        while (chcker != 1){
                            std::cin.clear();
                            /*std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');*/
                            std::getline(std::cin, decision);
                            if(decision == "1" || decision == "2" || decision == "3" || decision == "4"){
                                t_decision = std::stol(decision,nullptr,10);
                                chcker = 1;
                            }
                            else
                                std::cout << "That was not a valid choice.";
                        }
                        chcker = 0;
                        switch (t_decision)
                        {
                        case 4:
                            break;
                        case 3:
                            if (this->player->IsTired() == true){
                                std::cout << this->player->GetName() << " is too tired to cast." << std::endl;
                                i_decision = 4;
                                break;}
                            while (chcker != 1){
                                std::cout << "Which enemy?" << std::endl;
                                this->ListEnemies();
                                std::cin.clear();
                                /*std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');*/
                                std::getline(std::cin, decision);
                                if(decision == "1" || decision == "2" || decision == "3" || decision == "4" || decision == "5"){
                                    t_decision = std::stol(decision,nullptr,10);
                                    if (t_decision<=this->enemiesInTile.size())
                                        chcker = 1;
                                    else
                                        std::cout << "That was not a valid number of an enemy.";
                                }
                                else
                                    std::cout << "That was not a valid number of an enemy.";
                            }
                            //Player attacks Enemy
                            this->player->CastSpell(new BlindSpell(), this->enemiesInTile[t_decision-1]);
                            std::cout << this->enemiesInTile[t_decision-1]->GetName() << " starts to cry!" << std::endl;
                            this->player->Tired();
                            this->enemiesInTile[t_decision-1]->Blind();
                            playerTired = 2;
                            this->ListEnemies();
                            i_decision = 5;
                            chcker = 0;
                            break;
                        case 2:
                            if (this->player->IsTired() == true){
                                std::cout << this->player->GetName() << " is too tired to cast." << std::endl;
                                i_decision = 4;
                                break;}
                            this->player->CastSpell(new HealSpell, this->player);
                            std::cout << this->player->GetName() << " feels much better." << std::endl;
                            this->player->Tired();
                            playerTired = 2;
                            i_decision = 5;
                            break;
                        case 1:
                            if (this->player->IsTired() == true){
                                std::cout << this->player->GetName() << " is too tired to cast." << std::endl;
                                i_decision = 4;
                                break;}
                            while (chcker != 1){
                                std::cout << "Which enemy?" << std::endl;
                                this->ListEnemies();
                                std::cin.clear();
                                /*std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');*/
                                std::getline(std::cin, decision);
                                if(decision == "1" || decision == "2" || decision == "3" || decision == "4" || decision == "5"){
                                    t_decision = std::stol(decision,nullptr,10);
                                    if (t_decision<=this->enemiesInTile.size())
                                        chcker = 1;
                                    else
                                        std::cout << "That was not a valid number of an enemy.";
                                }
                                else
                                    std::cout << "That was not a valid number of an enemy.";
                            }
                            //Player attacks Enemy
                            this->player->CastSpell(new FireballSpell(), this->enemiesInTile[t_decision-1]);
                            std::cout << this->enemiesInTile[t_decision-1]->GetName() << " erupts in flames!" << std::endl;
                            this->player->Tired();
                            playerTired = 2;
                            this->ListEnemies();
                            i_decision = 5;
                            chcker = 0;
                            break;
                        default:
                            std::cout << "That was not 1, 2, or 3." << std::endl;
                            break;}
                    }
                }
                break;
            case 1:
                if (i_decision == 1){
                    while (chcker != 1){
                        std::cout << "Which enemy?" << std::endl;
                        this->ListEnemies();
                        std::cin.clear();
                        /*std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');*/
                        std::getline(std::cin, decision);
                        if(decision == "1" || decision == "2" || decision == "3" || decision == "4" || decision == "5"){
                            t_decision = std::stol(decision,nullptr,10);
                            if (t_decision<=this->enemiesInTile.size())
                                chcker = 1;
                            else
                                std::cout << "That was not a valid number of an enemy.";
                        }
                        else
                            std::cout << "That was not a valid number of an enemy.";
                    }
                    //Player attacks Enemy
                    this->player->DealDamage(this->enemiesInTile[t_decision-1]);
                    this->ListEnemies();
                    i_decision = 5;
                    chcker = 0;
                    break;
                }
            default:
                std::cout << "That was not 1, 2, 3, 4, or 5.  Please enter 1, 2, 3, 4, or 5 to proceed unless..." << std::endl;
                break;
            }
        }
        //        this->player->ModifyHealth(-1*this->player->GetHealing());
        for (CombatUnit* enemy : this->enemiesInTile) {
            //            enemy->ModifyHealth(enemy->GetHealing());
            if(enemy->GetName()=="OgreMage" || enemy->GetName()=="FemaleOgreMage"){
                if(enemy->GetHealth()<10&&charTired==0){
                    enemy->ModifyHealth(-20);
                    enemy->Tired();
                    charTired=5;
                    std::cout << "The OgreMage is suddenly rejuvenated!" << std::endl;
                    std::cout << "The OgreMage looks tired!" << std::endl << std::endl;
                }
                else if(charTired==0 && enemy->IsBlind()==false){
                    this->player->ModifyHealth(15);
                    enemy->Tired();
                    charTired=5;
                    std::cout << "The OgreMage blasts " << this->player->GetName() << "!" << std::endl;
                    std::cout << "The OgreMage looks tired!" << std::endl << std::endl;
                }
                else
                    continue;
            }
            else if(enemy->GetName()=="BegPaOgre"){
                if(enemy->GetHealth()<10&&bossTired==0){
                    enemy->ModifyHealth(-20);
                    enemy->Tired();
                    bossTired=2;
                    std::cout << "BegPaOgre is suddenly rejuvenated!" << std::endl;
                    std::cout << "The BegPaOgre looks tired!" << std::endl << std::endl;
                }
                else if(bossTired==0){
                    this->player->ModifyHealth(25);
                    enemy->Tired();
                    bossTired=4;
                    std::cout << "The BegPaOgre blasts " << this->player->GetName() << "!" << std::endl;
                    std::cout << "The BegPaOgre looks tired!" << std::endl << std::endl;
                }
                else
                    continue;
            }
            if (enemy->IsBlind()==false)
            {
            	enemy->DealDamage(this->player);
            }
            enemy->ModifyHealth(-1*enemy->GetHealing());
        }
        this->player->ModifyHealth(-1*this->player->GetHealing());
        for (CombatUnit* unit : this->enemiesInTile){
            while (unit->GetHealth()>unit->GetMaxHealth())
                unit->ModifyHealth(1);
        }
        //        while (this->player->GetHealth()>this->player->GetMaxHealth())
        //            this->player->ModifyHealth(1);
        this->player->PrintStats();
        this->ListEnemies();
        if (this->player->GetHealth()<1){
            this->player->SetIsWallopped(true);
            std::cout << this->player->GetName() << " has been walloped! Bummer Dude! Play Again?" << std::endl;
        }
        if (run == 1 && this->player->GetHealth()>0){
            std::cout << this->player->GetName() << " begins to run!" << std::endl;
            break;
        }
    }   //end of combat
    if (run == 1 && this->player->GetIsWallopped()==false){
        std::cout << this->player->GetName() << " has run!" << std::endl;
        return;
    }
    else if(this->player->GetIsWallopped()==true){
        return;
    }
    else{
        for (Item* item : this->itemsInTile) {
            std::cin.clear();
            std::string decision = "";
            while (decision!="y" || decision!="n"){
                std::cout << "Add " << item->GetName() << " to inventory? (y or n)" << std::endl;
                std::getline(std::cin, decision);
                if (decision == "y"){
                    this->player->AddItemToInventory(item);
                    break;
                }
                else if (decision == "n")
                    break;
                else
                    std::cout << "That was not yes or no." << std::endl;
            }
        }
    }
    std::cout << std::endl << "Player Inventory" << std::endl;
    this->player->PrintInventory();
    std::cout << std::endl << "Player Stats" << std::endl;
    this->player->PrintStats();
    this->SetPlayer(this->player);
    this->enemiesInTile.clear();
    this->itemsInTile.clear();
    std::cin.clear();
    /*std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');*/
}
